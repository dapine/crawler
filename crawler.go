package main

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"strings"

	"gopkg.in/xmlpath.v2"
)

var url = os.Args[1]
var files []string

const (
	chan4_op = "/html/body/form[@id='delform']/div" +
		"/div[@class='thread']/div/div/div[@class='file']/a/@href"
	chan4_anon = "/html/body/form[@id='delform']/div" +
		"/div[@class='thread']/div[@class='postContainer replyContainer']" +
		"/div[@class='post reply']/div[@class='file']/a/@href"
)

func main() {
	r := fetch(url)

	path_op := xmlpath.MustCompile(chan4_op)

	path := xmlpath.MustCompile(chan4_anon)

	root, err := xmlpath.ParseHTML(r)
	if err != nil {
		fmt.Println("error parse reader")
	}

	// Add OP's file to slice
	if value, ok := path_op.String(root); ok {
		files = append(files, value)
	}

	iter := path.Iter(root)

	files := extractFilesURL(iter)

	downloadFiles(files)
}

func fetch(url string) *bytes.Reader {
	res, _ := http.Get(url)
	defer res.Body.Close()

	x, err := ioutil.ReadAll(res.Body)
	if err != nil {
		fmt.Println("error reading body")
	}

	r := bytes.NewReader(x)

	return r
}

func extractFilesURL(iter *xmlpath.Iter) []string {
	files := []string{}

	for iter.Next() {
		files = append(files, iter.Node().String())
	}

	for i := 0; i < len(files); i++ {
		files[i] = files[i][2:]
	}

	return files
}

func downloadFile(fileURL string) {
	token := strings.Split(fileURL, "/")
	filename := token[len(token)-1]

	file, _ := os.Create(filename)
	defer file.Close()

	res, _ := http.Get("http://" + fileURL)
	defer res.Body.Close()

	_, err := io.Copy(file, res.Body)
	if err != nil {
		fmt.Println("Erro saving file")
	}

	fmt.Println("Downloading:", filename)
}

func downloadFiles(files []string) {
	for _, v := range files {
		downloadFile(v)
	}
}
